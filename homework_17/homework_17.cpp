﻿#include <iostream>
#include <string>
#include <cmath>

class MyClass {
private:
    std::string name="unknown";
public:
    void setName() {
        std::cout << "Enter name: " << std::endl;
        std::cin >> name;
    }

    std::string getName() {
        return name;
    }
};

class Vector {
private:
    double x=0;
    double y=0;
    double z=0;
public:
    Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z) 
    {}

    void printXYZ() {
        std::cout << "x: " << x << ", y: " << y << ", z: " << z << std::endl;
    }

    double getLength() {
        double result;
        result = std::pow(x, 2) + std::pow(y, 2) + std::pow(z, 2);
        result = std::abs(std::sqrt(result));
        return result;
    }
};

int main()
{
    //Part 1
    MyClass myClass;
    myClass.setName();
    std::cout << "Result: " << myClass.getName() << std::endl;

    //Part 2
    Vector vector(5, 5, 5);
    vector.printXYZ();
    std::cout << "Length: " << vector.getLength()<<std::endl;
}
